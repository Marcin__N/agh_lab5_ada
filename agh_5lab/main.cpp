#include <stdio.h>
#include <string.h>
#include <iostream>
#include <cstdlib>
#include <time.h>
#include <cmath>
#include <fstream>
#include <ctime>
#include <windows.h>

using namespace std;

int ilosc_wzorcow;
const int SIZE_TAB = 5000;  // d�ugo�� �a�cucha s
const int M = 3;  // d�ugo�� wzorca p
char  tablica[SIZE_TAB][SIZE_TAB];

void wczytanie() {
	for (int i = 0; i<SIZE_TAB; i++) {
		for (int j = 0; j<SIZE_TAB; j++) {
			cin >> tablica[i][j];
		}
		cin.ignore(10);
	}
}

void wczytaj_plik()
{
	fstream plik;
	plik.open("5000_pattern.txt", ios::in);
	if (plik.good() == true)
	{
		for (int i = 0; i<SIZE_TAB; i++)
		{
			for (int j = 0; j<SIZE_TAB; j++)
			{
				plik >> tablica[i][j];
				//cout << tablica[i][j];
			}
			//cout << endl;
			plik.get();
		}
		plik.close();
	}
}

bool sprawdzWPionie(int indexX, int indexY) {
	if (!(tablica[indexX + 1][indexY] == 'B'))
		return false;
	else if (!(tablica[indexX + 2][indexY] == 'C'))
		return false;
	return true;
}

bool sprawdzWPoziomie(int indexX, int indexY) {
	if (!(tablica[indexX][indexY + 1] == 'B'))
		return false;
	else if (!(tablica[indexX][indexY + 2] == 'C'))
		return false;
	return true;
}

void szukajWzorca() {
	for (int i = 0; i<SIZE_TAB - 2; i++) {
		for (int j = 0; j<SIZE_TAB - 2; j++) {
			if (tablica[i][j] == 'A') {
				if (sprawdzWPionie(i, j)) {
					if (sprawdzWPoziomie(i, j)) {
						//cout << "wspolrzedne punktu" << i << ":" << j << "\n";
						ilosc_wzorcow++;
						//	std::cout<<"Tablica :"<<tablica[i][j]<<tablica[i+1][j]<<tablica[i+2][j]<<tablica[i][j+1]<<tablica[i][j+2]<<"\n";
					}
				}

			}
		}

	}
}


int modulo(int a, int b)			// a % b
{
	int modulo = a;
	while (modulo >= b)
		modulo -= b;
	while (modulo < 0)
		modulo += b;
	return modulo;
}

int oblicz_hasz(char * tab, int start_indeks, int il_liter=M)
{
	int hasz=0;
	for (int i = il_liter -1; i >= 0; i--)
	{
		if (tab[start_indeks + i] < 58)
			hasz += pow(16, i) * (tab[start_indeks + il_liter - i - 1] - 48);
		else
			hasz += pow(16, i) * (tab[start_indeks + il_liter - i - 1] - 55);
		//cout << tab[start_indeks + il_liter - i - 1];
	}
	//cout << endl << "Moduloooo:   " << modulo(hasz, 23) << endl << endl;
	return modulo(hasz, 5507);
}

void wczytaj()
{
	for (int i = 0; i<SIZE_TAB; i++)
	{
		for (int j = 0; j<SIZE_TAB; j++)
		{
			cin >> tablica[i][j];
		}
	}
}


int main()
{
	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2, t3, t4;           // ticks
	double elapsedTime;
	QueryPerformanceFrequency(&frequency);

	ilosc_wzorcow = 0;
	wczytaj_plik();

	// START POMIAR CZAS
	QueryPerformanceCounter(&t1);

	szukajWzorca();

	QueryPerformanceCounter(&t2);
	// KONIEC POMIAR CZAS
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	cout << endl << "Ilosc wzorcow - algorytm nijaki: " << ilosc_wzorcow << endl;
	cout << "Czas poszukiwania wzorcow: " << elapsedTime << " ms." << endl;

	ilosc_wzorcow = 0;
	QueryPerformanceCounter(&t3);
	int hasz_wzoru = oblicz_hasz("ABC", 0);
	int hasz_okna;

	for (int i = 0; i < SIZE_TAB - 2; i++)
	{
		hasz_okna = oblicz_hasz(&tablica[i][0], 0);
		for (int j = 0; j < SIZE_TAB - 2; j++)
		{
			if (hasz_okna == hasz_wzoru && tablica[i][j] == 'A')
			{
				if (sprawdzWPionie(i, j) && sprawdzWPoziomie(i, j))
					ilosc_wzorcow++;
			}
			hasz_okna = oblicz_hasz(&tablica[i][j], 1);
			//if (tablica[i][j] < 58 && j < SIZE - 3)
				//hasz_okna = modulo(modulo(modulo(hasz_okna - (tablica[i][j] - 48) * 3, 5507) * 16, 5507) + (tablica[i][j+3] - 48), 5507);
			//else if (j < SIZE -3)
				//hasz_okna = modulo(modulo(modulo(hasz_okna - (tablica[i][j] - 55) * 3, 5507) * 16, 5507) + (tablica[i][j+3] - 55), 5507);
		}
	}
	QueryPerformanceCounter(&t4);
	// KONIEC POMIAR CZAS
	elapsedTime = (t4.QuadPart - t3.QuadPart) * 1000.0 / frequency.QuadPart;

	cout << endl << "Ilosc wzorcow - algorytm Rabina-Karpa: " << ilosc_wzorcow << endl;
	cout << "Czas poszukiwania wzorcow: " << elapsedTime << " ms." << endl;


	cin.get();
	cin.get();
	return 0;
}